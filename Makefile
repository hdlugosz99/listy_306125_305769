CC=gcc
CFLAGS=-Wall 
LIBS=-lm

main: main.o klient.o
	$(CC) $(CFLAGS) -o main main.o klient.o $(LIBS)

main.o: main.c moduly.h
	$(CC) $(CFLAGS) -c main.c

klient.o: klient.c
	$(CC) $(CFLAGS) -c klient.c


	